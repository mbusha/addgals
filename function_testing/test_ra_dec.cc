#include <stdio.h>
#include <math.h>
#include <iostream.h>
#include <string>
#include <cstdlib>
#include "chealpix.h"

int main()
{
  double x, y, z, ra, dec, angle_const;
  long HaloPixel;
  float theta, phi;
  double PI=3.1415926;

  x = -1034.04; 
  y = 638.273;
  z = 104.341;
  angle_const = 45.0/atan(1.0);

  ra = angle_const*atan2(y,x);
  dec = angle_const*asin(z/sqrt(x*x+y*y+z*z));
  cout<<"Initial ra/dec: "<<ra<<"/"<<dec<<endl;
  if(ra<0) ra = ra+360;
  cout<<"Transformed ra: "<<ra<<endl;
  theta = (90.0-dec)*PI/180.0;
  phi = ra*PI/180.0;
  //ang2pix_ring(4, theta, phi, &HaloPixel);
  //cout<<"theta = "<<theta<<", phi = "<<phi<<", pixel = "<<HaloPixel<<endl;
}
