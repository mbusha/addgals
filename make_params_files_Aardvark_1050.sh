#!/bin/sh

ZMIN=$1
ZMAX=$2
OPATH=$3
MMAG=$4
DENSFILE=$5
LBCGFILE=$6
PHISTAR=$7
PIXELNUM=$8
ZNUM=$9
OUTDIR=${10}

STRINGFILE=StringParameters
NUMFILE=NumericalParameters

cp StringParameters_Aardvark_1050_template $STRINGFILE
cp NumericalParameters_Aardvark_1050_template $NUMFILE

echo "ZREDMIN $ZMIN" >> $NUMFILE
echo "ZREDMAX $ZMAX" >> $NUMFILE
echo "Magmin $MMAG" >> $NUMFILE
echo "out_path $OPATH" >> $STRINGFILE 
echo "path $OPATH" >> $STRINGFILE 
echo "denspdffile $DENSFILE" >> $STRINGFILE
echo "lbcgfile $LBCGFILE" >> $STRINGFILE
echo "simulationfile  /lustre/ki/pfs/mbusha/projects/Aardvark/Lb1050/output/snapshot_Lightcone_000_healpix_$PIXELNUM" >> $STRINGFILE

echo "rnnfile         /lustre/ki/pfs/mbusha/projects/Aardvark/Lb1050/rnn/rnn_snapshot_Lightcone_000_healpix_$PIXELNUM" >> $STRINGFILE

echo "phistar $PHISTAR" >> $NUMFILE
echo "PixelNum $PIXELNUM" >> $NUMFILE

mkdir -p ${OUTDIR}/idl
cp hv $OUTDIR
cp StringParameters $OUTDIR
cp NumericalParameters $OUTDIR
cp tLF.dat ${OUTDIR}/LF.dat

IDL_FILE=${OUTDIR}/idl/run.idl
GO_FILE=${OUTDIR}/idl/go.sh
NAME=PO_Aardvark_1050_${PIXELNUM}.${ZNUM}

echo ".compile ./add_bcgs.pro" > $IDL_FILE
echo "name = 'PO_Aardvark_1050_${PIXELNUM}.${ZNUM}'" >> $IDL_FILE
echo "create_catalog, name, g, h, /des,/vista,/deep,/johnson,/flamex" >> $IDL_FILE
echo "exit" >> $IDL_FILE
echo "cp /afs/slac.stanford.edu/u/ki/mbusha/projects/addgals/idl/make_catalog.sav ." > $GO_FILE
echo "/afs/slac.stanford.edu/u/ki/mbusha/bin/idl_vm_run.py make_catalog.sav $NAME" >> $GO_FILE
chmod 744 $GO_FILE

