vector <float> GetNeighborDist(vector <Galaxy *> galaxies, vector <Galaxy *> points){
  cout<<"In GetNeighborDist"<<endl;
  cout<<"  number of galaxies: "<<galaxies.size()<<endl;;
  cout<<"  number of points:   "<<points.size()<<endl;
  PRNTV(zmax->GetVal());
  //PRNTV(galaxies.size());
  int nbins = (int) (ceil(zmax->GetVal()/zbsize));
  PRNTV(nbins);
  vector <int> ginbin(nbins+1);
  for(int i=0;i<ginbin.size();i++){
    ginbin[i] = 0;
  } 
  int OutNext = 0;
  cout<<" initial ginbin's: "<<ginbin[0]<<" "<<ginbin[1]<<" "<<ginbin[2]<<" "<<ginbin[3]<<" "<<ginbin[4]<<endl; 
  for(int i=0;i<galaxies.size();i++){
    if(i>OutNext){
      //cout<<i<<" of "<<galaxies.size()<<endl;;
      OutNext += 1000;
    }
    ginbin[galaxies[i]->Zbin()]++;
    if (i < 5){
      cout<<galaxies[i]->Ra()<<" "<<galaxies[i]->Dec()<<" "<<galaxies[i]->Z()<<" "<<galaxies[i]->Zbin()<<" "<<ginbin[galaxies[i]->Zbin()]<<endl;
    }
    
  int ginanybin = 0;
  for(int i=0;i<ginbin.size();i++){
    ginanybin += ginbin[i];
    //cout<<"  "<<i<<" "<<ginbin[i]<<endl;
  } 
  cout<<" Number of galaxies in bins (should only be ngals if Magmin >= Magmin_dens): "<<ginanybin<<endl;

  vector <int> pinbin(nbins+1);
  for(int i=0;i<pinbin.size();i++){
    pinbin[i] = 0;
  } 
  cout<<"  Zeroed pinbin."<<endl;
  OutNext = 0;
  for(int i=0;i<points.size();i++){
    //cout<<i<<" of "<<galaxies.size()<<endl;
    if(i>OutNext){
      //cout<<i<<" of "<<galaxies.size()<<endl;;
      OutNext += 1000;
    }
    pinbin[points[i]->Zbin()]++;
    if (i < 5){
      cout<<points[i]->Ra()<<" "<<points[i]->Dec()<<" "<<points[i]->Z()<<" "<<points[i]->Zbin()<<" "<<pinbin[points[i]->Zbin()]<<endl;
    }
  } 
  cout<<"  Set pinbin"<<endl;
  int pinanybin = 0;
  for(int i=0;i<pinbin.size();i++)
    pinanybin += pinbin[i];
  cout<<" Number of points in bins: "<<pinanybin<<endl;

#ifdef DEBUG
  cout<<"[neighbor] "<<nbins<<" z bins"<<endl;
#endif
  int startbin=0;
  bool have_started = false;
  for(int i=0;i<nbins;i++){
#ifdef DEBUG
        cout<<"[neighbor] "<<i<<" "<<ginbin[i]<<endl;
#endif
        //      if((ginbin[i]<10)&&(i>1)){
        if(ginbin[i]<10){
          if(have_started == false){
            startbin++;
            //cout<<"not enough objects in bin "<<i<<endl;
          }
          else{
            //cout<<"not enough objects in bin "<<i<<endl;
            nbins = i;
            break;
          }
        }
        else have_started = true;
  }
  cout<<"using "<<startbin<<" "<<nbins<<endl;
  //vector <float> nndist(galaxies.size());
  vector <float> nndist(points.size());
  ANNpoint            query_pt;               // query point
  ANNidxArray         nn_idx;                 // near neighbor indices
  ANNdistArray        dists;                  // near neighbor distances
  ANNkd_tree * the_tree;
  double eps=0;
  int dim = 2;
  int k_want = 10;  // 10th nearest neighbor (first one is self)
  if(dens_measure == FIFTH)
    k_want = 5;

  query_pt = annAllocPt(dim);             // allocate query point
  //  int jmax = 0;
  ofstream ddout("dd2.out");
  ofstream nfout("nf.out");
  //loop over the z slices
  int  pi=0;
#ifdef DEBUG
  cout<<"Looping over "<<nbins-startbin<<"z bins"<<endl;
#endif
  int max_search= 350;

  for(int bi=startbin;bi<nbins;bi++){
    ANNpointArray       data_pts;        // data points to look for neighbors
    ANNpointArray       data_pts1;       // data points to measure dists for
    //int npts=ginbin[bi];
    int npts=pinbin[bi];
    if (npts == 0)
      continue;
    //cout<<"npts:"<<npts<<endl;
    int nsearch=ginbin[bi];
    //cout<<" bin has "<<npts<<" bright gals and "<<nsearch<<" total gals."<<endl;
    int k_search = 70;                   //initial search radius
    //if(npts<k_search) k_search=npts-1;
    if(nsearch<k_search) k_search=nsearch-1;
    nn_idx = new ANNidx[k_search];          // allocate near neigh indices
    dists = new ANNdist[k_search];          // allocate near neighbor dists

    data_pts1 = annAllocPts(npts, dim);         // allocate data points
    vector <int> ids(npts);
    vector <int> sids;
    sids.reserve(nsearch);
    if(bi>0) nsearch+=ginbin[bi-1];
    if(bi<ginbin.size()) nsearch+=ginbin[bi+1];
    data_pts = annAllocPts(nsearch, dim);         // allocate data points
    int nsi = 0;
    int npi = 0;

    //cout<<"Searching through bin "<<bi<<endl;
    //cout<<"  allocated nsearch = "<<nsearch<<endl;

//    for(int i=0;i<galaxies.size();i++){
//      if((galaxies[i]->Zbin()==bi)||
//       (galaxies[i]->Zbin()==bi-1)||
//       (galaxies[i]->Zbin()==bi+1)){
//      assert(nsi<nsearch);
//      data_pts[nsi][0] = galaxies[i]->Ra();
//      data_pts[nsi][1] = galaxies[i]->Dec();
//      sids.push_back(i);
//      nsi++;
//      }
//      if(galaxies[i]->Zbin()==bi){
//      data_pts1[npi][0] = galaxies[i]->Ra();
//      data_pts1[npi][1] = galaxies[i]->Dec();
//      ids[npi]=i;
//      npi++;
//      } 
//    }
    for(int i=0;i<galaxies.size();i++){
      if((galaxies[i]->Zbin()==bi)||
         (galaxies[i]->Zbin()==bi-1)||
         (galaxies[i]->Zbin()==bi+1)){
        assert(nsi<nsearch);
        data_pts[nsi][0] = galaxies[i]->Ra();
        data_pts[nsi][1] = galaxies[i]->Dec();
        sids.push_back(i);
        nsi++;
      }
    }
    for(int i=0;i<points.size();i++){
      if(points[i]->Zbin()==bi){
        data_pts1[npi][0] = points[i]->Ra();
        data_pts1[npi][1] = points[i]->Dec();
        ids[npi]=i;
        npi++;
      }
    }

    //cout<<"The Tree."<<endl;
    the_tree = new ANNkd_tree(
                              data_pts,  // the data points
                              nsearch,   // number of points
                              dim);                   // dimension of space
    //cout<<"bi = "<<bi<<" of "<<nbins<<" npts = "<<npts<<" npi = "<<npi<<" nsi = "<<nsi<<endl;
    for(int i=0;i<npts;i++){
      //cout<<"Searching for "<<i<<" of "<<npts<<endl;
      int gid = ids[i];
      #ifdef DEBUG
      if(pi%50000==0) {cout<<bi<<" "<<i<<" "<<pi<<endl; system("date");}
      #endif
      bool distfound = false;
      ANNpoint query_pt = data_pts1[i];
      float dist=0;
      int n_found = -1;
      int t_found = -1;
      while(!distfound){
        //if(npts<k_search) k_search=npts-1;
        //if(nsi<k_search) k_search=npts-1;
        if(nsearch<k_search) k_search=nsearch-1;
        if(k_search>max_search) k_search=max_search;
        int nn = 0;
        //cout<<"doing search with k_search = "<<k_search<<endl;
        the_tree->annkSearch(                   // search
                             query_pt,          // query point
                             k_search,          // number of near neighbors
                             nn_idx,            // nearest neighbors (returned)
                             dists,             // distance (returned)
                             eps);              // error bound  
        for(int j=0;j<k_search;j++){
          int chosen_id = sids[nn_idx[j]];
          //  if((gid!=chosen_id)
          //&&(fabs(galaxies[gid]->Z()-galaxies[chosen_id]->Z())<delta_z)){
          //if((fabs(galaxies[gid]->Z()-galaxies[chosen_id]->Z())<delta_z)){
          if((fabs(points[gid]->Z()-galaxies[chosen_id]->Z())<delta_z)){
            ++nn;
          }
          if(nn==k_want){
            //for(int jjj=0;jjj<j;jjj++){
            //cout<<galaxies[gid]->ComovDist(sqrt(dists[jjj]))<<" ";
            //}
            //cout<<endl;
            //dist = galaxies[gid]->ComovDist(sqrt(dists[j]));
            dist = points[gid]->ComovDist(sqrt(dists[j]));
            n_found = j;
            t_found = 0;
            distfound = true;
            break;
          }
        }
        if(!distfound){
          //dist = galaxies[pi]->ComovDist(sqrt(dists[k_search-1]));
          dist = points[pi]->ComovDist(sqrt(dists[k_search-1]));
          //if you're at the end, pretend you've found it
          //if((k_search==npts-1)||(k_search>=max_search)){
          if((k_search==nsearch-1)||(k_search>=max_search)){
            n_found = k_search-1;
            t_found = 1;
            distfound = true;
          }
          //if you're big already, pretend you've found it
          //      if(dist>15){
          if(dist>10){
            n_found = -1;
            t_found = 2;
            distfound = true;
          }
          else{
            //nfout<<dist<<" "<<i<<" "<<galaxies[i]->Z()<<" "<<k_search<<endl;
            nfout<<dist<<" "<<i<<" "<<points[i]->Z()<<" "<<k_search<<endl;
            //re-search if you didn't look far out enough
            k_search = 2*k_search;
            delete [] nn_idx;
            delete [] dists;
            nn_idx = new ANNidx[k_search];
            dists = new ANNdist[k_search];
          }
        }//if dist is not found
      }//while dist is not found
      nndist[gid] = dist;
      ddout<<i<<" "<<pi<<" "<<gid<<" "<<dist<<" "<<t_found<<" "<<n_found<<" "<<k_search<<" "<<npts<<" "<<bi<<endl;
      pi++;
    }//loop over points in z bin
    //    ofstream ddout("dd2.out");
    //nnout<<nndist[i]<<endl;
    delete the_tree;
    delete [] nn_idx;
    delete [] dists;
  }//loop over bins
  //MSG("Exiting neighbor");
  return nndist;
}

