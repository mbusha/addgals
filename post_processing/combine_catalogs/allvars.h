
#define zColumnNo 5

typedef struct 
{
  int id;
  int ecatid;
  float omag[5];
  float amag[5];
  float z;
  float ra;
  float dec;
  float px;
  float py;
  float pz;
  float vx;
  float vy;
  float vz;
  int edge;
  int haloid;
  float m200;
  int ngals;
  float r200;
  float rhalo;
  int central;
  float mstar;
  float d8;
  float nndist;
  float nnpercent;
} galaxy_in;

extern char InPath[200], FileBaseName[200], OutPath[200];
extern int nFiles;

extern int nGalaxies;
