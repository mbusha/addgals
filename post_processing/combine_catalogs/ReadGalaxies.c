#include <stdio.h>
#include <stdlib.h>
#include "fitsio.h"
#include "allvars.h"

void check_status(char *file, int status)
{
  if (status != 0)
    {
      printf("Error!  Status = %d on file `%s'\n", status, file);
      exit(10);
    }
}

//count the number of galaxies we're intersted in
long CountGalaxies(float *zmin, float *zmax)
{
  fitsfile *fg1, *fg2;
  char g_file1[200], g_file2[200];
  long nrows1, nrows2;
  int i, j;
  int status = 0;
  float *zarr1, *zarr2;
  int nulval = 0, anynul = 0;
  float tzmin, tzmax;
  long GalaxyCount = 0;
  int tGalaxyCount;

  zmin[0] = -1.0;
  zmax[nFile-1] = 
  for(i=0;i<nFiles-1;i++)
    {
      sprintf(g_file1,"%s/%03d/idl/%s_%03d_galaxies.fit",InPath,i,FileBaseName,i);
      sprintf(g_file2,"%s/%03d/idl/%s_%03d_galaxies.fit",InPath,i+1,FileBaseName,i+1);
      fits_open_data(&fg1, g_file1, READONLY, &status);
      fits_report_error(stdout, status);
      if (status != 0)
        exit(2);
      fits_open_data(&fg2, g_file2, READONLY, &status);
      fits_report_error(stdout, status);
      if (status != 0)
        exit(3);

      //count the rows to get the number of galaxies
      fits_get_num_rows(fg1, &nrows1, &status);
      zarr1 = malloc(sizeof(float)*nrows1);
      fits_get_num_rows(fg2, &nrows2, &status);
      zarr2 = malloc(sizeof(float)*nrows2);

      //Read the galaxy redshfits
      printf("Reading %ld and %ld redshifts in files %d and %d...", nrows1, nrows2, i, i+1);
      fits_read_col(fg1, TFLOAT, zColumnNo, 1, 1, nrows1, &nulval, 
		    zarr1, &anynul, &status);
      fits_read_col(fg2, TFLOAT, zColumnNo, 1, 1, nrows2, &nulval, 
		    zarr2, &anynul, &status);

      fits_close_file(fg1, &status);
      fits_close_file(fg2, &status);

      //determine the min/max redshifts for the files
      tzmin = 100;
      tzmax = -100;
      for(j=0;j<nrows1;j++)
	if (zarr1[j] > tzmax)
	  tzmax = zarr1[j];
      for(j=0;j<nrows2;j++)
	if (zarr2[i] < tzmin)
	  tzmin = zarr2[j];
      zmax[i] = 0.5*(tzmin + tzmax);
      zmin[i+1] = zmax[i];

      //count the nubmer of galaxiese in the redsshfit range
      tGalaxyCount = 0;
      for(j=0;j<nrows1;j++)
	if (zarr1[j] > zmin[i] && zarr1[j] <= zmax[i])
	  {
	    GalaxyCount++;
	    tGalaxyCount++;
	  }

      printf("file will contribute %d galaxies.\n", tGalaxyCount);
      free(zarr1);
      free(zarr2);
    }
  return GalaxyCount;
}


//routine to read in the galaxy files
void ReadGalaxies(void)
{
  fitsfile *fg, *fsdss, *fdeep, *fdes, *fjohnson, *fflamex, *fvista;
  char g_file[200], sdss_file[200], deep_file[200], des_file[200];
  char johnson_file[200], vista_file[200], flamex_file[200];
  char g_file1[200], g_file2[200];
  int i, ig, status;
  int hdutype, ncolumns;
  long nrows;
  char card[FLEN_CARD];
  int *tint;
  int nulval, anynul;
  float *zmin, *zmax;

  anynul = 0;
  status = 0;
  zmin = malloc(sizeof(float)*nFiles);
  zmax = malloc(sizeof(float)*nFiles);
  nGalaxies = CountGalaxies(zmin, zmax);
  for(i=0;i<nFiles;i++)
    {
      printf("Reading file %d of %d...\n", i, nFiles);
      sprintf(g_file,"%s/%03d/idl/%s_%03d_galaxies.fit",InPath,i,FileBaseName,i);
      sprintf(sdss_file,"%s/%03d/idl/%s_%03d_sdss25.fit",InPath,i,FileBaseName,i);
      sprintf(deep_file,"%s/%03d/idl/%s_%03d_deep.fit",InPath,i,FileBaseName,i);
      sprintf(des_file,"%s/%03d/idl/%s_%03d_des.fit",InPath,i,FileBaseName,i);
      sprintf(johnson_file,"%s/%03d/idl/%s_%03d_johnson.fit",InPath,i,FileBaseName,i);
      sprintf(vista_file,"%s/%03d/idl/%s_%03d_vista.fit", InPath, i, FileBaseName, i);
      sprintf(flamex_file,"%s/%03d/idl/%s_%03d_flamex.fit",InPath,i,FileBaseName,i);
  
      fits_open_data(&fg, g_file, READONLY, &status);
      fits_get_num_rows(fg, &nrows, &status);
      fits_get_num_cols(fg, &ncolumns, &status);
      fits_report_error(stdout, status);
      printf("   file has %ld rows and %d columns...\n", nrows, ncolumns);
      int nulval = -1;
      tint = malloc(sizeof(int)*nrows);
      fits_read_col(fg, TINT, 1, 1, 1, nrows, &nulval, tint, &anynul, &status);
      fits_close_file(fg, &status);
      fits_report_error(stdout, status);
      check_status(g_file, status);
      //tgal = fits_read_table(g_file);
    }
}
