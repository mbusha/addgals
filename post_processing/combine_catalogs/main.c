#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "allvars.h"
#include "proto.h"

int main(int argc, char **argv)
{
  char ParamFile[200];

  if(argc != 2) 
    {
      printf("Error!  Did not specify a parameter file!\n");
      exit(0);
    }

  //read in our parameter file
  strcpy(ParamFile, argv[1]);
  ReadParamFile(ParamFile);
  
  //Read in our galaxy distribution
  ReadGalaxies();

  //Add "Process" the catalogs and save
  //Process_dr8();
  //Process_stripe82();
  //Process_des();

}
